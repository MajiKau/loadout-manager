#pragma once

#include <SdkHeaders.h>
#include <nlohmann/json.hpp>


/**
 * Version=1,TitleID=61000,
	TopIconID=64017,TopColorID=64752,MiddleIconID=64640,MiddleColorID=64750,BottomIconID=64740,BottomColorID=64750,
	PSkinID=-1,PWeaponID=40003,PMuzzleID=43014,PBarrelID=41003,PMagazineID=44009,PScopeID=45009,
		PStockID=42007,PGripID=-1,PAmmoID=90000,PWeaponCamoID=630300,
	SSkinID=-1,SWeaponID=40001,SMuzzleID=43007,SBarrelID=41020,SMagazineID=44054,SScopeID=45018,
		SStockID=42000,SGripID=-1,SAmmoID=90000,SWeaponCamoID=630300,
	WeaponHangerID=5000,
	BodyCamoID=630100,UpperBodyID=30200,LowerBodyID=30106,HelmetID=30300,BadgeID=46000,
	Gear_R1ID=12015,Gear_R2ID=12015,Gear_L1ID=12000,Gear_L2ID=12011,TacticalID=13000,
	ButtPackID=12015,bFemale=0,AvatarID=-1,GearHangerID=-1,
	DepotID0=20001,DepotID1=20000,DepotID2=20003,DepotID3=20011,DepotID4=20004,
	TauntID0=360,TauntID1=360,TauntID2=360,TauntID3=360,TauntID4=360,TauntID5=360,TauntID6=360,TauntID7=360,
	DialogAnnouncerID=63000,DialogPlayerID=63050
*/
namespace BLRE::Loadout
{
	struct EmblemConfig
	{
		int TopIcon;
		int TopColor;
		int MiddleIcon;
		int MiddleColor;
		int BottomIcon;
		int BottomColor;

		std::string ToDataString();
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(EmblemConfig,
		TopIcon, TopColor, MiddleIcon, MiddleColor, BottomIcon, BottomColor);

	struct BodyConfig
	{
		int UpperBody;
		int LowerBody;
		int Helmet;
		int Badge;
		int Camo;
		int Avatar;
		bool Female;
		int ButtPack;

		std::string ToDataString();
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(BodyConfig,
		UpperBody, LowerBody, Helmet, Badge, Camo, Avatar, Female, ButtPack);

	struct GearConfig
	{
		int R1;
		int R2;
		int L1;
		int L2;
		int Tactical;
		int Hanger;

		std::string ToDataString();
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(GearConfig,
		R1, R2, L1, L2, Tactical, Hanger);

	struct WeaponConfig
	{
		int Receiver;
		int Muzzle;
		int Barrel;
		int Magazine;
		int Scope;
		int Stock;
		int Grip;
		int Ammo;
		int Camo;
		int Skin;

		std::string ToDataString(bool primary);
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(WeaponConfig,
		Receiver, Muzzle, Barrel, Magazine, Scope, Stock, Grip, Ammo, Camo, Skin);

	struct LoadoutConfig
	{
		std::string Name;
		std::string PrimaryFriendlyName;
		std::string SecondaryFriendlyName;

		int Title;
		EmblemConfig Emblem;
		WeaponConfig Primary;
		WeaponConfig Secondary;
		int WeaponHanger;
		BodyConfig Body;
		GearConfig Gear;
		int Depot[4];
		int Taunts[8];
		int DialogAnnouncer;
		int DialogPlayer;

		std::string DepotDataString();
		std::string TauntsDataString();
		std::string ToDataString();
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(LoadoutConfig,
		Name, Title, PrimaryFriendlyName, SecondaryFriendlyName,
		Primary, Secondary, WeaponHanger,
		Body, Gear, Depot, Taunts, Emblem,
		DialogAnnouncer, DialogPlayer);
}
