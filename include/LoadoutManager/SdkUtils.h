#pragma once

#include <nlohmann/json.hpp>
#include <SdkHeaders.h>

bool operator==(const FProfileTauntInfo& lhs, const FProfileTauntInfo& rhs);
bool operator!=(const FProfileTauntInfo& lhs, const FProfileTauntInfo& rhs);

bool operator==(const FProfileDepotInfo& lhs, const FProfileDepotInfo& rhs);
bool operator!=(const FProfileDepotInfo& lhs, const FProfileDepotInfo& rhs);

bool operator==(const FProfileGearInfo& lhs, const FProfileGearInfo& rhs);
bool operator!=(const FProfileGearInfo& lhs, const FProfileGearInfo& rhs);

bool operator==(const FProfileLoadoutSlot& lhs, const FProfileLoadoutSlot& rhs);
bool operator!=(const FProfileLoadoutSlot& lhs, const FProfileLoadoutSlot& rhs);


void to_json(nlohmann::json& j, const FFoxWeaponConfigInfo& config);
void from_json(const nlohmann::json& j, FFoxWeaponConfigInfo& config);

void to_json(nlohmann::json& j, const FProfileLoadoutSlot& loadoutSlot);
void from_json(const nlohmann::json& j, FProfileLoadoutSlot& loadoutSlot);

void to_json(nlohmann::json& j, const FProfileGearInfo& gear);
void from_json(const nlohmann::json& j, FProfileGearInfo& gear);