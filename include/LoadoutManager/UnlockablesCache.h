#pragma once
#include <unordered_map>
#include <SdkHeaders.h>
#include <BLRevive/Component.h>

namespace BLRE::Loadout
{
	/**
	 * @brief Cache for unlockable items (necessary since `UFoxDataStore_Unlockables` seems to be broken)
	*/
	class UnlockablesCache : BLRE::Component
	{
	public:
		UnlockablesCache();

		/**
		 * @brief build unlockable cache info (uid to index map)
		*/
		void BuildCache();

		/**
		 * @brief convert item UIDs to data provider indices in loadout info
		 * @param loadoutData loadout info to convert uids on
		*/
		void ConvertUIDsToIndices(FLoadoutInfoData& loadoutData);

		/**
		* @brief get data provider index from UID
		* @param UID uid to convert to index
		* @param defaultIndex index to use if UID is invalid
		*/
		int GetProviderIndexFromUID(int UID, int defaultIndex);

		UFoxWeaponModObjectBase* GetModObjectFromUID(int UID) {
			auto mod = WeaponMods.find(UID);
			if (mod == WeaponMods.end())
				return nullptr;

			return mod->second;
		}

	protected:
		std::unordered_map<int, UFoxDataProvider_Unlockable*> UnlockInfos = {};
		std::unordered_map<int, UFoxWeaponModObjectBase*> WeaponMods = {};
		std::unordered_map<int, int> UIDToIndexMap = {};

		/**
		 * @brief cache indices of weapons by provider
		 * @param weaponNames weapon provider array
		 * @param weapons list of weapons
		*/
		void CacheWeaponProviderIndices(TArray<FString> weaponNames, std::vector<AFoxWeapon*> weapons);
		
		void CacheWeaponModProviderIndices(TArray<FString> modNames, int modUidBase);
		void CacheScopeProviderIndices();
		void CacheBarrelProviderIndices();
		void CacheAmmoProviderIndices();
		void CacheStockProviderIndices();
		void CacheGripProviderIndices();

		/**
		 * @brief cache data provider indices
		 * @tparam T data provider class
		 * @param provider data provider
		*/
		template<typename T>
		void CacheDataProviderIndices(TArray<T*>* provider)
		{
			CacheDataProviderIndices(reinterpret_cast<TArray<UFoxDataProvider_Unlockable*>*>(provider));
		}

		/**
		 * @brief cache data provider indices
		 * @param provider data provider
		*/
		void CacheDataProviderIndices(TArray<UFoxDataProvider_Unlockable*>* provider);

		template<typename TUnlockInfo>
			requires(std::is_base_of_v<FBaseUnlockInfo, TUnlockInfo>)
		void CacheUnlockInfoIndices(TArray<TUnlockInfo>* provider)
		{
			for (int i = 0; i < provider->Count; i++)
			{
				auto info = provider->at(i);
				UIDToIndexMap[info.UnlockID] = i;
			}
		}

		/**
		 * @brief generate `FLoadoutInfoData::WeaponData` from `FLoadoutInfoData::WeaponUnlocks`
		 * @param loadoutData loadout info to convert
		*/
		void ConvertWeaponData(FLoadoutInfoData& loadoutData);

		/**
		 * @brief generate `FLoadoutInfoData::GearData` from `FLoadoutInfoData::GearUnlocks`
		 * @param loadoutData loaodut info to convert
		*/
		void ConvertGearData(FLoadoutInfoData& loadoutData);

		/**
		 * @brief generate `FLoadoutInfoData::DepotData` from `FLoadoutInfoData::DepotUnlocks`
		 * @param loadoutData loadout info to convert
		*/
		void ConvertDepotData(FLoadoutInfoData& loadoutData);
	};
}