#pragma once
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <string>
#include <map>
#include <vector>
#include <functional>
#include <mutex>
#include <nlohmann/json.hpp>

#include <BLRevive/BLRevive.h>
#include <BLRevive/Utils.h>
#include <LoadoutManager/PlayerLoadout.h>
#include <BLRevive/Detours/FunctionDetour.h>

namespace BLRE::Loadout
{
	/**
	 * Abstract loadout manager
	 */
	class LoadoutManager : public BLRE::Component
	{
	public:
		// directory path of profile files
		inline static const std::string ProfileDirectory = BLRE::Utils::FS::BlreviveConfigPath() + "\\profiles\\";

		// logger instance
		inline static BLRE::LogFactory::TSharedLogger StaticLog = nullptr;;

		/**
		 * Initialize loadout manager
		 */
		virtual void Initialize();

		/**
		 * Check for default files and create them if not existant
		 */
		void CheckDefaultFiles();

		LoadoutManager(BLRevive* pBlre)
			: Component("LoadoutManager")
		{ 
			blre = pBlre;
		}

	protected:
		BLRevive* blre;
	};
}
