#include <LoadoutManager/ClientManager.h>
#include <LoadoutManager/UnlockablesCache.h>

using namespace BLRE::Loadout;

ClientManager::ClientManager(BLRE::BLRevive* blre)
	: LoadoutManager(blre)
{

}

void ClientManager::Initialize()
{
	Log->debug("initializing client loadout");

	auto dtSetupLoadout = Detours::FunctionDetour::Create("FoxGame.FoxPC.SetupLoadout", this, &ClientManager::SetupLoadout);
	dtSetupLoadout->Enable();
	
	auto dtValidityCheckLoadoutInfo = Detours::FunctionDetour::Create(
		"FoxGame.FoxPC.ChangeSelectedLoadoutByID", this, &ClientManager::ChangeSelectedLoadoutByID);
	dtValidityCheckLoadoutInfo->Enable();

	Log->info("initialized client loadout manager");
}

void DETOUR_CB_CLS_IMPL(ClientManager::ChangeSelectedLoadoutByID, AFoxPC, ChangeSelectedLoadoutByID, pc)
{
	detour->Skip();

	auto loadoutInfo = pc->GetLoadoutInfo();
	auto loadoutData = CachedLoadouts->GetCachedLoadoutInfo(params->NewLoadoutID.ToChar(), pc);

	loadoutInfo->CurrentLoadoutID = loadoutData.LoadoutId;
	loadoutInfo->CurrentLoadoutIndex = loadoutData.LoadoutIndex;
	loadoutData.BoostModifierInfoData = loadoutInfo->BoostModifierInfo;
	loadoutInfo->SetLoadoutInfo(loadoutData);
	auto pri = (AFoxPRI*)pc->PlayerReplicationInfo;
	pri->CurrentTitleUnlockID = loadoutData.TitleId;
	pri->CurrentEmblem = loadoutData.EmblemData;

	pc->PushLoadout(loadoutData);
}

void DETOUR_CB_CLS_IMPL(ClientManager::SetupLoadout, AFoxPC, SetupLoadout, pc)
{
	static bool firstRun = true;

	auto staticDataStore = UObject::GetInstanceOf<UFoxDataStore_StoreData>(true);

	pc->ProfileSettings->eventSetLoadoutsMigrated(true);
	pc->ProfileSettings->SetDefaultLoadoutID("loadout-0");
	pc->ProfileSettings->SetDefaultLoadoutPVEID("loadout-0");

	if (firstRun)
	{
		Log->info("initializing loadout");

		UFoxDataStore_StoreData* dataStore;
		if (!staticDataStore->eventGetDataStore(&dataStore))
		{
			Log->error("couldnt get datastore instance");
			return;
		}

		dataStore->CachedLoginStatus = ELoginStatus::LS_LoggedIn;

		UFoxItemCacheLoadouts* loadoutCache;
		if (!dataStore->eventGetCachedLoadouts(&loadoutCache))
		{
			Log->debug("creating loadout cache");
			dataStore->LoadoutsCache = UObject::CreateInstance<UFoxItemCacheLoadouts>();
			loadoutCache = dataStore->LoadoutsCache;
		}

		CachedLoadouts = std::make_unique<LoadoutCache>();
		CachedLoadouts->LoadProfileFile(this->blre->URL.GetParam("Name", "default"), pc);

		firstRun = false;
	}
}
