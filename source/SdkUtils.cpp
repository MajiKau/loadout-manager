#include <LoadoutManager/SdkUtils.h>
#include <BLRevive/BLRevive.h>

using json = nlohmann::json;

static BLRE::LogFactory::TSharedLogger UtilsLog = BLRE::BLRevive::GetInstance()->LogFactory->Create("SdkUtils");

static AFoxLoadoutInfo* GetDefaultLoadoutInfo()
{
	static AFoxLoadoutInfo* DefaultLoadoutInfo = nullptr;

	if (DefaultLoadoutInfo == nullptr)
		DefaultLoadoutInfo = UObject::GetDefaultInstanceOf<AFoxLoadoutInfo>();

	return DefaultLoadoutInfo;
}

static UFoxDataStore_Unlockables* GetUnlockablesDataStore()
{
	static UFoxDataStore_Unlockables* DefaultUnlockablesDataStore = nullptr;

	if (DefaultUnlockablesDataStore == nullptr)
		DefaultUnlockablesDataStore = UObject::GetDefaultInstanceOf<UFoxDataStore_Unlockables>();

	return DefaultUnlockablesDataStore;
}

bool operator==(const FProfileTauntInfo& lhs, const FProfileTauntInfo& rhs)
{
	return lhs.TauntID0 == rhs.TauntID0 &&
		lhs.TauntID1 == rhs.TauntID1 &&
		lhs.TauntID2 == rhs.TauntID2 &&
		lhs.TauntID3 == rhs.TauntID3 &&
		lhs.TauntID4 == rhs.TauntID4 &&
		lhs.TauntID5 == rhs.TauntID5 &&
		lhs.TauntID6 == rhs.TauntID6 &&
		lhs.TauntID7 == rhs.TauntID7;
}

bool operator!=(const FProfileTauntInfo& lhs, const FProfileTauntInfo& rhs)
{
	return !(lhs == rhs);
}

bool operator==(const FProfileDepotInfo& lhs, const FProfileDepotInfo& rhs)
{
	return lhs.DepotPresetID0 == rhs.DepotPresetID0 &&
		lhs.DepotPresetID1 == rhs.DepotPresetID1 &&
		lhs.DepotPresetID2 == rhs.DepotPresetID2 &&
		lhs.DepotPresetID3 == rhs.DepotPresetID3 &&
		lhs.DepotPresetID4 == rhs.DepotPresetID4;
}

bool operator!=(const FProfileDepotInfo& lhs, const FProfileDepotInfo& rhs)
{
	return !(lhs == rhs);
}


bool operator==(const FProfileGearInfo& lhs, const FProfileGearInfo& rhs)
{
	return lhs.BodyCamoID == rhs.BodyCamoID &&
		lhs.UpperBodyID == rhs.UpperBodyID &&
		lhs.LowerBodyID == rhs.LowerBodyID &&
		lhs.HelmetID == rhs.HelmetID &&
		lhs.BadgeID == rhs.BadgeID &&
		lhs.Gear_R1ID == rhs.Gear_R1ID &&
		lhs.Gear_R2ID == rhs.Gear_R2ID &&
		lhs.Gear_L1ID == rhs.Gear_L1ID &&
		lhs.Gear_L2ID == rhs.Gear_L2ID &&
		lhs.TacticalID == rhs.TacticalID &&
		lhs.ButtPackID == rhs.ButtPackID &&
		lhs.bFemale == rhs.bFemale &&
		lhs.bBot == rhs.bBot &&
		lhs.AvatarID == rhs.AvatarID &&
		lhs.PatchIconID == rhs.PatchIconID &&
		lhs.PatchIconColorID == rhs.PatchIconColorID &&
		lhs.PatchShapeID == rhs.PatchShapeID &&
		lhs.PatchShapeColorID == rhs.PatchShapeColorID &&
		lhs.HangerID == rhs.HangerID;
}

bool operator!=(const FProfileGearInfo& lhs, const FProfileGearInfo& rhs)
{
	return !(lhs == rhs);
}

bool operator==(const FProfileLoadoutSlot& lhs, const FProfileLoadoutSlot& rhs)
{
	return lhs.PrimarySkinID == rhs.PrimarySkinID &&
		lhs.PrimaryWeaponID == rhs.PrimaryWeaponID &&
		lhs.PrimaryMuzzleID == rhs.PrimaryMuzzleID &&
		lhs.PrimaryBarrelID == rhs.PrimaryBarrelID &&
		lhs.PrimaryMagazineID == rhs.PrimaryMagazineID &&
		lhs.PrimaryScopeID == rhs.PrimaryScopeID &&
		lhs.PrimaryStockID == rhs.PrimaryStockID &&
		lhs.PrimaryGripID == rhs.PrimaryGripID &&
		lhs.PrimaryAmmoID == rhs.PrimaryAmmoID &&
		lhs.PrimaryWeaponCamo == rhs.PrimaryWeaponCamo &&
		lhs.SecondarySkinID == rhs.SecondarySkinID &&
		lhs.SecondaryWeaponID == rhs.SecondaryWeaponID &&
		lhs.SecondaryMuzzleID == rhs.SecondaryMuzzleID &&
		lhs.SecondaryMagazineID == rhs.SecondaryMagazineID &&
		lhs.SecondaryScopeID == rhs.SecondaryScopeID &&
		lhs.SecondaryBarrelID == rhs.SecondaryBarrelID &&
		lhs.SecondaryStockID == rhs.SecondaryStockID &&
		lhs.SecondaryGripID == rhs.SecondaryGripID &&
		lhs.SecondaryAmmoID == rhs.SecondaryAmmoID &&
		lhs.SecondaryWeaponCamo == rhs.SecondaryWeaponCamo &&
		lhs.HangerID == rhs.HangerID;
}

bool operator!=(const FProfileLoadoutSlot& lhs, const FProfileLoadoutSlot& rhs)
{
	return !(lhs == rhs);
}

void to_json(json& j, const FFoxWeaponConfigInfo& config)
{
	auto UDS = GetUnlockablesDataStore();

	j = json{
		{"Receiver", config.WeaponClass == NULL ? -1 : ((AFoxWeapon*)config.WeaponClass)->UnlockID},
		{"Barrel", config.WeaponBarrel == NULL ? -1 : ((UFoxWeaponBarrel*)config.WeaponBarrel)->UnlockID},
		{"Scope", config.WeaponScope == NULL ? -1 : ((UFoxWeaponScope*)config.WeaponScope)->UnlockID},
		{"Grip", config.WeaponGrip == NULL ? -1 : ((UFoxWeaponGrip*)config.WeaponGrip)->UnlockID},
		{"Stock", config.WeaponStock == NULL ? -1 : ((UFoxWeaponStock*)config.WeaponStock)->UnlockID},
		{"Ammo", config.WeaponAmmo == NULL ? -1 : ((UFoxWeaponAmmo_Base*)config.WeaponAmmo)->UnlockID},

		{"Muzzle", ((UFoxDataProvider_Unlockable*)UDS->MuzzleProviderArray.Data[config.WeaponMuzzle])->UnlockID},
		{"Magazine", ((UFoxDataProvider_Unlockable*)UDS->MagazineProviderArray.Data[config.WeaponMagazine])->UnlockID},
		{"CamoIndex", (int)config.WeaponCamoIndex},
		{"Skin", (int)config.WeaponSkin},
		{"Hanger", ((UFoxDataProvider_Unlockable*)UDS->HangerProviderArray.Data[config.WeaponHanger])->UnlockID} };
}

void from_json(const json& j, FFoxWeaponConfigInfo& config)
{
	AFoxLoadoutInfo* LoadoutInfo = GetDefaultLoadoutInfo();
	UFoxDataStore_Unlockables* UDS = GetUnlockablesDataStore();

	if (!LoadoutInfo || !UDS)
	{
		UtilsLog->error("Failed to parse weapon config because provider is missing.");
		return;
	}

	auto GetWeaponsProviderById = [=]<typename T>(int UID, TArray<T*> arr)
	{
		for (int i = 0; i < arr.Count; i++)
		{
			if (arr.Data[0] && arr.Data[0]->IsA(UFoxDataProvider_Unlockable::StaticClass()) && ((UFoxDataProvider_Unlockable*)arr.Data[i])->UnlockID == UID)
				return i;
		}
		return 0;
	};

	config.WeaponClass = LoadoutInfo->GetWeaponClassById((int)j["Receiver"]);
	config.WeaponBarrel = LoadoutInfo->eventGetModClassById((int)j["Barrel"], UT_Barrel);
	config.WeaponScope = LoadoutInfo->eventGetModClassById((int)j["Scope"], UT_Scope);
	config.WeaponGrip = LoadoutInfo->eventGetModClassById((int)j["Grip"], UT_Grip);
	config.WeaponStock = LoadoutInfo->eventGetModClassById((int)j["Stock"], UT_Stock);
	config.WeaponAmmo = LoadoutInfo->eventGetModClassById((int)j["Ammo"], UT_Ammo);

	config.WeaponMuzzle = GetWeaponsProviderById((int)j["Muzzle"], UDS->MuzzleProviderArray);
	config.WeaponMagazine = GetWeaponsProviderById((int)j["Magazine"], UDS->MagazineProviderArray);
	config.WeaponCamoIndex = (int)j["CamoIndex"];
	config.WeaponSkin = (int)j["Skin"];
	config.WeaponHanger = GetWeaponsProviderById((int)j["Hanger"], UDS->HangerProviderArray);
}

void to_json(json& j, const FProfileLoadoutSlot& loadoutSlot)
{
	j = {
		{"Primary", {
			{"Receiver", loadoutSlot.PrimaryWeaponID},
			{"Barrel", loadoutSlot.PrimaryBarrelID},
			{"Scope", loadoutSlot.PrimaryScopeID},
			{"Grip", loadoutSlot.PrimaryGripID},
			{"Stock", loadoutSlot.PrimaryStockID},
			{"Ammo", loadoutSlot.PrimaryAmmoID},
			{"Muzzle", loadoutSlot.PrimaryMuzzleID},
			{"Magazine", loadoutSlot.PrimaryMagazineID},
			{"CamoIndex", loadoutSlot.PrimaryWeaponCamo},
			{"Skin", loadoutSlot.PrimarySkinID},
			{"Hanger", loadoutSlot.HangerID}
		}},
		{"Secondary", {
			{"Receiver", loadoutSlot.SecondaryWeaponID},
			{"Barrel", loadoutSlot.SecondaryBarrelID},
			{"Scope", loadoutSlot.SecondaryScopeID},
			{"Grip", loadoutSlot.SecondaryGripID},
			{"Stock", loadoutSlot.SecondaryStockID},
			{"Ammo", loadoutSlot.SecondaryAmmoID},
			{"Muzzle", loadoutSlot.SecondaryMuzzleID},
			{"Magazine", loadoutSlot.SecondaryMagazineID},
			{"CamoIndex", loadoutSlot.SecondaryWeaponCamo},
			{"Skin", loadoutSlot.SecondarySkinID},
		}}
	};
}

void from_json(const json& j, FProfileLoadoutSlot& loadoutSlot)
{
	loadoutSlot.PrimaryWeaponID = j["Primary"]["Receiver"];
	loadoutSlot.PrimaryBarrelID = j["Primary"]["Barrel"];
	loadoutSlot.PrimaryScopeID = j["Primary"]["Scope"];
	loadoutSlot.PrimaryGripID = j["Primary"]["Grip"];
	loadoutSlot.PrimaryStockID = j["Primary"]["Stock"];
	loadoutSlot.PrimaryAmmoID = j["Primary"]["Ammo"];
	loadoutSlot.PrimaryMuzzleID = j["Primary"]["Muzzle"];
	loadoutSlot.PrimaryMagazineID = j["Primary"]["Magazine"];
	loadoutSlot.PrimaryWeaponCamo = j["Primary"]["CamoIndex"];
	loadoutSlot.PrimarySkinID = j["Primary"]["Skin"];
	loadoutSlot.HangerID = j["Primary"]["Hanger"];
	loadoutSlot.SecondaryWeaponID = j["Secondary"]["Receiver"];
	loadoutSlot.SecondaryBarrelID = j["Secondary"]["Barrel"];
	loadoutSlot.SecondaryScopeID = j["Secondary"]["Scope"];
	loadoutSlot.SecondaryGripID = j["Secondary"]["Grip"];
	loadoutSlot.SecondaryStockID = j["Secondary"]["Stock"];
	loadoutSlot.SecondaryAmmoID = j["Secondary"]["Ammo"];
	loadoutSlot.SecondaryMuzzleID = j["Secondary"]["Muzzle"];
	loadoutSlot.SecondaryMagazineID = j["Secondary"]["Magazine"];
	loadoutSlot.SecondaryWeaponCamo = j["Secondary"]["CamoIndex"];
	loadoutSlot.SecondarySkinID = j["Secondary"]["Skin"];
}

void to_json(json& j, const FProfileGearInfo& gear)
{
	j = {
		{"Female", gear.bFemale},
		{"Bot", gear.bBot},
		{"BodyCamo", gear.BodyCamoID},
		{"UpperBody", gear.UpperBodyID},
		{"LowerBody", gear.LowerBodyID},
		{"Helmet", gear.HelmetID},
		{"Badge", gear.BadgeID},
		{"Gear_R1", gear.Gear_R1ID},
		{"Gear_R2", gear.Gear_R2ID},
		{"Gear_L1", gear.Gear_L1ID},
		{"Gear_L2", gear.Gear_L2ID},
		{"Tactical", gear.TacticalID},
		{"ButtPack", gear.ButtPackID},
		{"Avatar", gear.AvatarID},
		{"PatchIcon", gear.PatchIconID},
		{"PatchIconColor", gear.PatchIconColorID},
		{"PatchShape", gear.PatchShapeID},
		{"PatchShapeColor", gear.PatchShapeColorID},
		{"Hanger", gear.HangerID} };
}

void from_json(const json& j, FProfileGearInfo& gear)
{
	gear.bFemale = j["Female"].get<bool>();
	gear.bBot = j["Bot"].get<bool>();
	j.at("BodyCamo").get_to(gear.BodyCamoID);
	j.at("UpperBody").get_to(gear.UpperBodyID);
	j.at("LowerBody").get_to(gear.LowerBodyID);
	j.at("Helmet").get_to(gear.HelmetID);
	j.at("Badge").get_to(gear.BadgeID);
	j.at("Gear_R1").get_to(gear.Gear_R1ID);
	j.at("Gear_R2").get_to(gear.Gear_R2ID);
	j.at("Gear_L1").get_to(gear.Gear_L1ID);
	j.at("Gear_L2").get_to(gear.Gear_L2ID);
	j.at("Tactical").get_to(gear.TacticalID);
	j.at("ButtPack").get_to(gear.ButtPackID);
	j.at("Avatar").get_to(gear.AvatarID);
	j.at("PatchIcon").get_to(gear.PatchIconID);
	j.at("PatchIconColor").get_to(gear.PatchIconColorID);
	j.at("PatchShape").get_to(gear.PatchShapeID);
	j.at("PatchShapeColor").get_to(gear.PatchShapeColorID);
	j.at("Hanger").get_to(gear.HangerID);
}
