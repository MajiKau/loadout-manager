#include <LoadoutManager/PlayerLoadout.h>
#include <LoadoutManager/SdkUtils.h>
#include <spdlog/fmt/fmt.h>

using namespace BLRE::Loadout;
using namespace fmt::literals;
using json = nlohmann::json;

std::string EmblemConfig::ToDataString()
{
	return fmt::format(
		"TopIconID={0},TopColorID={1},MiddleIconID={2},MiddleColorID={3},BottomIconID={4},BottomColorID={5}",
		TopIcon, TopColor, MiddleIcon, MiddleColor, BottomIcon, BottomColor);
}

std::string BodyConfig::ToDataString()
{
	return fmt::format(
		"BodyCamoID={0},UpperBodyID={1},LowerBodyID={2},HelmetID={3},BadgeID={4}",
		Camo, UpperBody, LowerBody, Helmet, Badge);
}

std::string GearConfig::ToDataString()
{
	return fmt::format(
		"Gear_R1ID={0},Gear_R2ID={1},Gear_L1ID={2},Gear_L2ID={3},TacticalID={4}",
		R1, R2, L1, L2, Tactical);
}

std::string WeaponConfig::ToDataString(bool primary)
{
	std::string prefix = primary ? "P" : "S";

	return fmt::format(
		"{0}SkinID={1},{0}WeaponID={2},{0}MuzzleID={3},{0}BarrelID={4},{0}MagazineID={5},{0}ScopeID={6},{0}StockID={7},{0}GripID={8},{0}AmmoID={9},{0}WeaponCamoID={10}",
		prefix, Skin, Receiver, Muzzle, Barrel, Magazine, Scope, Stock, Grip, Ammo, Camo);
}

std::string LoadoutConfig::DepotDataString()
{
	return fmt::format(
		"DepotID0={},DepotID1={},DepotID2={},DepotID3={},DepotID4={}",
		Depot[0], Depot[1], Depot[2], Depot[3], Depot[4]);
}

std::string LoadoutConfig::TauntsDataString()
{
	return fmt::format(
		"TauntID0={},TauntID1={},TauntID2={},TauntID3={},TauntID4={},TauntID5={},TauntID6={},TauntID7={}",
		Taunts[0], Taunts[1], Taunts[2], Taunts[3], Taunts[4], Taunts[5], Taunts[6], Taunts[7]);
}

std::string LoadoutConfig::ToDataString()
{
	auto emblemDataStr = Emblem.ToDataString();
	auto primaryDataStr = Primary.ToDataString(true);
	auto secondaryDataStr = Secondary.ToDataString(false);
	auto gearDataStr = Gear.ToDataString();
	auto bodyDataStr = Body.ToDataString();
	auto depotDataStr = DepotDataString();
	auto tauntsDataStr = TauntsDataString();

	return fmt::format("Version=1,TitleID={},{},{},{},WeaponHangerID={},{},{},ButtPackID={},bFemale={},AvatarID={},GearHangerID={},{},{},{},DialogAnnouncerID={},DialogPlayerID={}",
		Title, emblemDataStr, primaryDataStr, secondaryDataStr, WeaponHanger,
		bodyDataStr, gearDataStr, Body.ButtPack, Body.Female ? 1 : 0, Body.Avatar, Gear.Hanger, gearDataStr, depotDataStr, tauntsDataStr, DialogAnnouncer, DialogPlayer);
}